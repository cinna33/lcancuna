const menuIcon = document.querySelector(".hamburger-menu");
const navbar = document.querySelector(".navbar");

const line1 = document.querySelector(".line-1");
const line2 = document.querySelector(".line-2")
menuIcon.addEventListener("click", () => {
  
    navbar.classList.toggle("change");
     
    line1.classList.toggle("line-1-rotate")
    line2.classList.toggle("line-2-rotate")
});




/*CURSEUR*/

let mouseCursor = document.querySelector(".cursor");
window.addEventListener("mousemove", cursor);

function cursor(e) {
    mouseCursor.style.top = e.pageY + "px"
    mouseCursor.style.left = e.pageX + "px"
}


